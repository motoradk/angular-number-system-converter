'use strict';

let gulp = require('gulp');
let browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
  browserSync({
    open: true,
    server: {
      baseDir: './src',
      port: 3000,
    },
    https: false
  });
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});
