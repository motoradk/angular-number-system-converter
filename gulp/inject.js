'use strict';

let gulp = require('gulp');
let wiredep = require('wiredep').stream;

gulp.task('wiredep', function() {
  return require('wiredep')({
    src: ['src/index.html', 'karma.conf.js']
  })
});
