'use strict';

let jshint = require('gulp-jshint');
let gulp = require('gulp');

gulp.task('jshint', function() {
  return gulp.src('src/app/**/*.js')
    .pipe(jshint())
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
});
