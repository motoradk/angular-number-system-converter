'use strict';

let gulp = require('gulp');
let plumber = require('gulp-plumber');
let jshint = require('gulp-jshint');
let concat = require('gulp-concat');
let browserSync = require('browser-sync');

gulp.task('scripts', function(){
  return gulp.src(['src/app/**/*.js', '!src/app/**/*.spec.js']).pipe(plumber({
    errorHandler: function (error) {
      console.log(error.message);
      this.emit('end');
    }}))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('src/dist/scripts/'))
    .pipe(browserSync.reload({stream:true}));
});
