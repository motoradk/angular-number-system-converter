'use strict';

let gulp = require('gulp');
let templateCache = require('gulp-angular-templatecache');
let browserSync = require('browser-sync');

gulp.task('template-cache', function () {
  return gulp.src('src/app/**/*.html')
    .pipe(templateCache({ module:'App', standalone:false }))
    .pipe(gulp.dest('src/dist/scripts'))
    .pipe(browserSync.reload({stream:true}));
});
