'use strict';

let gulp = require('gulp');
let Server = require('karma').Server;

gulp.task('test', function (done) {
  new Server({
    configFile: require('path').resolve('karma.conf.js'),
    singleRun: true
  }, done).start();
});
