'use strict';

let fs = require('fs');
let gulp = require('gulp');
let jshint = require('gulp-jshint');

fs.readdirSync('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});

gulp.task('default', ['clean', 'template-cache', 'wiredep', 'jshint', 'scripts', 'browser-sync'], function() {
  gulp.watch('bower.json', ['wiredep']);
  gulp.watch('src/index.html', ['bs-reload']);
  gulp.watch('src/app/**/*.html', ['template-cache']);
  gulp.watch('src/app/**/*.js', ['scripts']);
});
