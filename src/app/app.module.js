(function () {
  'use strict';

  angular
    .module('App', [
      'ui.router',
      'ngSanitize',
      'number-system-converter'
    ]);
})();
