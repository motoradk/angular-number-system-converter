(function() {
  'use strict';

  let scope,controller;

  describe('MainController', function() {
    beforeEach(module('App'));

    beforeEach(inject(function($controller){
      scope = {};
      controller = $controller('MainController', { $scope: scope });
    }));

    it('should be defined', function() {
      expect(controller).toBeDefined();
    });
  });
})();

