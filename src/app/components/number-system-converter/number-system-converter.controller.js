(function() {
  'use strict';

  angular
    .module('number-system-converter', [])
    .controller('NumberSystemConverterController', NumberSystemConverterController);

  NumberSystemConverterController.$inject=['$scope', 'numberSystemConverterService'];
  function NumberSystemConverterController($scope, numberSystemConverterService) {
    const REGEX_FOR_NUM = /^[0-9]+$/;
    const REGEX_WHITE_SPACES = /\s/g;

    $scope.showAlertMessageForEnterSysNumber = false;
    $scope.showAlertMessageForResultSysNumber = false;
    $scope.showAlertMessageForNumberToConvert = false;

    $scope.convertNumber = convertNumber;

    $scope.$watch('firstSysNumber', watchFirstSysNumberModel);
    $scope.$watch('resultSysNumber', watchResultSysNumberModel);
    $scope.$watch('numberToConvert', watchNumberToConvertModel);

    function watchNumberToConvertModel(newVal) {
      if(newVal) {
        $scope.showAlertMessageForNumberToConvert = !validInputData(newVal, REGEX_WHITE_SPACES);
      }
    }

    function watchFirstSysNumberModel(newVal) {
      if(newVal) {
        $scope.showAlertMessageForEnterSysNumber = validInputData(newVal, REGEX_FOR_NUM);
      }
    }

    function watchResultSysNumberModel(newVal) {
      if(newVal) {
        $scope.showAlertMessageForResultSysNumber = validInputData(newVal, REGEX_FOR_NUM);
      }
    }

    function validInputData(value, regex) {
      return !value.match(regex);
    }

    function convertNumber() {
      $scope.resultNumber = numberSystemConverterService.getConversionResult($scope.firstSysNumber, $scope.resultSysNumber, $scope.numberToConvert);
    }
  }
})();
