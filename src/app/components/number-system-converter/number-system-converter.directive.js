(function() {
  'use strict';

  angular
    .module('number-system-converter')
    .directive('numberSystemConverter', numberSystemConverter);

  function numberSystemConverter() {
    let directive = {
      restrict: 'E',
      templateUrl: 'components/number-system-converter/number-system-converter.view.html',
      controller: 'NumberSystemConverterController'
    };

    return directive;
  }
})();
