(function() {
  'use strict';

  angular
    .module('number-system-converter')
    .factory('numberSystemConverterService', numberSystemConverterService);

  numberSystemConverterService.$inject=['numberConverterValue'];
  function numberSystemConverterService(numberConverterValue) {
    let service = {
      getConversionResult : getConversionResult
    };

    return service;

    function getConversionResult(inputSystem, outputSystem, numberToConvert) {
      let tmp = 0;
      let result = '';

      for(let i=0; i<numberToConvert.length; i++) {
        let value = numberConverterValue[numberToConvert[i]];
        let basePower = (numberToConvert.length - 1) - i;

        tmp += value * Math.pow(inputSystem, basePower);
      }

      while(tmp > 0) {
        let remainder = tmp % outputSystem;

        result = Object.keys(numberConverterValue)[remainder] + result;
        tmp = Math.floor(tmp / outputSystem);
      }

      return result;
    }
  }
})();
