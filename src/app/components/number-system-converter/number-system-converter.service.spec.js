(function () {
  'use strict';

  describe('numberSystemConverterService', function () {
    let numberSystemConverterService;

    beforeEach(module('number-system-converter'));

    beforeEach(inject(function(_numberSystemConverterService_) {
      numberSystemConverterService = _numberSystemConverterService_;
    }));

    it('should be defined', function() {
      expect(numberSystemConverterService).toBeDefined();
    });

    it('should convert method be defined', function() {
      expect(numberSystemConverterService.getConversionResult).toBeDefined();
    });

    it('should convert from decimal to binary properly', function(){
      let inputSystem = '10';
      let outputSystem = '2';
      let numberToConvert = '128';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('10000000');
    });

    it('should convert from binary to decimal properly', function(){
      let inputSystem = '2';
      let outputSystem = '10';
      let numberToConvert = '10000000';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('128');
    });

    it('should convert from decimal to octonary properly', function() {
      let inputSystem = '10';
      let outputSystem = '8';
      let numberToConvert = '231';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('347');
    });

    it('should convert from octonary to decimal properly', function() {
      let inputSystem = '8';
      let outputSystem = '10';
      let numberToConvert = '347';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('231');
    });

    it('should convert from binary to octonary properly', function() {
      let inputSystem = '2';
      let outputSystem = '8';
      let numberToConvert = '100110';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('46');
    });

    it('should convert from octonary to binary properly', function() {
      let inputSystem = '8';
      let outputSystem = '2';
      let numberToConvert = '46';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('100110');
    });

    it('should convert from decimal to hexadecimal properly', function() {
      let inputSystem = '10';
      let outputSystem = '16';
      let numberToConvert = '4612';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('1204');
    });

    it('should convert from hexadecimal to decimal properly', function() {
      let inputSystem = '16';
      let outputSystem = '10';
      let numberToConvert = '1204';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('4612');
    });

    it('should convert from binary to hexadecimal properly', function() {
      let inputSystem = '2';
      let outputSystem = '16';
      let numberToConvert = '1001001';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('49');
    });

    it('should convert from hexadecimal to octonary properly', function() {
      let inputSystem = '16';
      let outputSystem = '8';
      let numberToConvert = '16D1';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('13321');
    });

    it('should convert from octonary to hexadecimal properly', function() {
      let inputSystem = '8';
      let outputSystem = '16';
      let numberToConvert = 'AB21';
      let numberAfterConvert = numberSystemConverterService.getConversionResult(inputSystem,outputSystem,numberToConvert);

      expect(numberAfterConvert).toBe('16D1');
    });
  });
})();
