(function () {
  'use strict';

  describe('numberConverterValue', function () {
    let numberConverterValue;

    beforeEach(module('number-system-converter'));

    beforeEach(inject(function (_numberConverterValue_) {
      numberConverterValue = _numberConverterValue_;
    }));

    it('should be defined', function () {
      expect(numberConverterValue).toBeDefined();
    });
  });
})();
